﻿$CheckDir = Test-Path "C:\InstallationFiles"

if($CheckDir -eq $true) {
    Write-Host "Directory already exists. Carry on."
} else {
    Write-Host "Directory does not exist. Creating Directory..."
    new-Item -itemtype directory -Path C:\InstallationFiles
}

# Gets DeployRoot, regardless of PXE or USB.
$tsenv = new-object -comobject Microsoft.SMS.TSEnvironment
$DeployRoot = $tsenv.Value("DeployRoot") 

Copy-Item -recurse "$DeployRoot\customtasks\InstallationFiles\*" "C:\InstallationFiles"

# Created tsenv. No longer need multiple folders. 2018-04-16
#Copy-Item "\\appserver01\it guy\xfer\rnr\NetScanner\64-bit\netscan.exe" "C:\InstallationFiles"
#Copy-Item "\\appserver01\it guy\xfer\rnr\CrystalReports-v11\cr11setup.exe" "C:\InstallationFiles"
#Copy-Item "\\appserver01\it guy\xfer\rnr\OFFICE 2003.zip"


#

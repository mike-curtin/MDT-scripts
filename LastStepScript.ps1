# Use the Set-NetConnectionProfile to change the location type # added 3:51 PM 8/11/2017
#Get-NetConnectionProfile | Set-NetConnectionProfile -NetworkCategory Private # Removed because it is not being joined to the Domain, with CustomSettings.ini, with initial MDT.

# Turns Legacy Default Printer Mode Settings On
reg add "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Windows" /v LegacyDefaultPrinterMode /t REG_DWORD /d 1 /f

# HIPAA Screen Saver Regulations (Type/Time-In-Seconds/Passsword_On_Wake)
reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v SCRNSAVE.EXE /t REG_SZ /d C:\Windows\system32\scrnsave.scr /f
reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v ScreenSaveTimeOut /t REG_SZ /d 300 /f
reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v ScreenSaverIsSecure /t REG_SZ /d 1 /f

# Underlines Shortcut Keys
reg add "HKEY_CURRENT_USER\Control Panel\Accessibility\Keyboard Preference" /v On /t REG_SZ /d 1 /f

# Turns Off PopUp Manager
reg add "HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\New Windows" /v PopupMgr /t REG_DWORD /d 0 /f

# Turn On Network Discovery
netsh firewall set service type=fileandprint mode=enable profile=all
netsh advfirewall firewall set rule group="Network Discovery" new enable=Yes

netsh advfirewall firewall set rule group="windows remote management" new enable=yes
netsh advfirewall firewall set rule group="windows management instrumentation (wmi)" new enable=yes
# [Next line must be run if it can run, kept for redundancy.]
netsh firewall set service type=remoteadmin mode=enable
netsh advfirewall firewall set rule group="remote administration" new enable=yes

# Turn On Network Discovery
netsh firewall set service type=fileandprint mode=enable profile=all
netsh advfirewall firewall set rule group="Network Discovery" new enable=Yes

# Sets Specific Time Zone
tzutil /s "Eastern Standard Time"

# Opens Performance Options Window (must set manually)
# DISABLED FOR MDT # start $env:windir\system32\SystemPropertiesPerformance.exe

##############################

# Add TrustedHosts
# MDT WILL GIVE ERROR # winrm quickconfig -force
# MDT WILL GIVE ERROR # set-item wsman:\localhost\Client\TrustedHosts -value * -force

# Remote WMI
netsh advfirewall firewall set rule group="Windows Management Instrumentation (WMI)" new enable=yes

# and Enabling PS-Remoting
Enable-PSRemoting

# Enable RDP

reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
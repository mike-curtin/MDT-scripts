:: Removes VLC Media Player, CCleaner, and Acrobat Reader DC shortcuts.
del "%HOMEDRIVE%\Users\Public\Desktop\Acrobat Reader DC.lnk"
:: del "%HOMEDRIVE%\Users\Public\Desktop\CCleaner.lnk"
:: CCleaner is not used as of 2017-09-19
del "%HOMEDRIVE%\Users\Public\Desktop\VLC media player.lnk"
del "%HOMEDRIVE%\Users\Public\Desktop\OpenOffice*.lnk"
del "%HOMEDRIVE%\Users\Public\Desktop\Google Chrome.lnk"

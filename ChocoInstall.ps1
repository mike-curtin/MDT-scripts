# ###########################################
# #   Install Chocolatey from Powershell    #
# ###########################################

Write-Host "Install Chocolatey from PowerShell"

# Old way to install # iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex 
# New way to install 1:48 PM 8/11/2017:

#Set-ExecutionPolicy RemoteSigned; 
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Upgrade Chocolatey

choco upgrade chocolatey

# Choco - Install applications

choco install googlechrome openoffice jre8 adobereader adobeair 7zip.install vlc er powershell adobereader-update silverlight dotnet4.0 dotnet4.5.1 flashplayerplugin cutepdf word.viewer powerpoint.viewer excel.viewer -y --ignore-package-exit-codes
#Removed 2017-08-11 # vcredist2005 vcredist2008 vcredist2010 vcredist2013 vcredist2012 vcredist2015 directx 
#Removed 2017-08-15, possibly causing MDT errors, script by itself gives errors on these two apps # adobereader-update revo.uninstaller
#Removed 2017-09-19 cclean. CCleanerhad a malware breach, will not take chances again.
# Choco - upgrade applications - same as all applications above.

choco upgrade googlechrome openoffice jre8 adobereader adobeair 7zip.install vlc powershell adobereader-update silverlight dotnet4.0 dotnet4.5.1 flashplayerplugin cutepdf word.viewer powerpoint.viewer excel.viewer -y --ignore-package-exit-codes
#Removed 2017-08-11 # vcredist2005 vcredist2008 vcredist2010 vcredist2013 vcredist2012 vcredist2015 directx

# Added to troubleshoot error codes at the end of the choco script (MDTs error codes/warnings)
Import-Module "$env:ChocolateyInstall\helpers\chocolateyInstaller.psm1" -Force; Get-Help Set-PowerShellExitCode -Full
Set-PowerShellExitCode 0 # Try to remove to get rid of "rc=1" after deployment. Removed 2017-09-25 # re-added. Changed exit code to '0'.